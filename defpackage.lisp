(defpackage :postgresql
  (:nicknames :pg)
  (:use :common-lisp
        :pg-md5
        #+cmu :alien
        #+cmu :c-call
        #+openmcl :ccl)
  #+openmcl (:shadow ccl:socket-connect)
  (:export #:pg-connect #:pg-exec #:pg-result #:pg-disconnect
	   #:pgcon-sql-stream
           #:*pg-disable-type-coercion*
           #:*pg-client-encoding*
           #:pg-databases #:pg-tables #:pg-columns
           #:pg-backend-version
           #:pg-date-style
           #:pg-client-encoding
           #:with-pg-connection
           #:with-pg-transaction
           #:pg-for-each
           #:pglo-create
           #:pglo-open
           #:pglo-close
           #:pglo-read
           #:pglo-write
           #:pglo-lseek
           #:pglo-tell
           #:pglo-unlink
           #:pglo-import
           #:pglo-export
           #:pg-supports-pbe
           #:pg-prepare
           #:pg-bind
           #:pg-execute
           #:pg-close-portal
           #:pg-close-statement
           #:postgresql-error
           #:connection-failure
           #:authentication-failure
           #:protocol-error
           #:backend-error))

;; EOF
